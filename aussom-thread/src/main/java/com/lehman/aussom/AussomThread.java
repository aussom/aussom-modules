/*
 * Copyright 2024 Austin Lehman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.lehman.aussom;

import com.aussom.Environment;
import com.aussom.ast.aussomException;
import com.aussom.types.*;
import com.aussom.stdlib.console;

import java.util.ArrayList;

public class AussomThread extends Thread {
	public AussomThread() {  }

	private AussomCallback ac = null;
	
	public AussomType setOnRun(Environment env, ArrayList<AussomType> args) {
		if(args.get(0).getType() == cType.cCallback) {
			this.ac = (AussomCallback) args.get(0);
		}
		return env.getClassInstance();
	}
	
	public AussomType startThread(Environment env, ArrayList<AussomType> args) {
		this.start();
		return env.getClassInstance();
	}
	
	public AussomType sleep(Environment env, ArrayList<AussomType> args) {
		long mills = ((AussomInt)args.get(0)).getValue();
		try {
			Thread.sleep(mills);
		} catch (InterruptedException e) {
			return new AussomException("thread.sleep(): Interrupted exception. " + e.getMessage());
		}
		return env.getClassInstance();
	}
	
	public AussomType interrupt(Environment env, ArrayList<AussomType> args) {
		this.interrupt();
		return env.getClassInstance();
	}
	
	public AussomType setPriority(Environment env, ArrayList<AussomType> args) {
		int priority = (int)((AussomInt)args.get(0)).getValue();
		
		try {
			this.setPriority(priority);
		} catch(IllegalArgumentException iae) {
			return new AussomException("thread.setPriority(): Illegal argument exception. Priority range is between " + String.valueOf(this.MIN_PRIORITY) + " and " + String.valueOf(this.MAX_PRIORITY) + ".");
		} catch(SecurityException se) {
			return new AussomException("thread.setPriority(): Threw Java Security Exception: " + se.getMessage());
		}
		
		return env.getClassInstance();
	}
	
	public AussomType setName(Environment env, ArrayList<AussomType> args) {
		String name = ((AussomString)args.get(0)).getValueString();
		
		try {
			this.setName(name);
		} catch(SecurityException se) {
			return new AussomException("thread.setName(): Threw Java Security Exception: " + se.getMessage());
		}
		
		return env.getClassInstance();
	}
	
	public AussomType setDaemon(Environment env, ArrayList<AussomType> args) {
		boolean daemon = ((AussomBool)args.get(0)).getValue();
		
		try {
			this.setDaemon(daemon);
		} catch(IllegalThreadStateException itse) {
			return new AussomException("thread.setDaemon(): Illegal thread state exception. " + itse.getMessage());
		} catch(SecurityException se) {
			return new AussomException("thread.setDaemon(): Security exception. " + se.getMessage());
		}
		
		return env.getClassInstance();
	}

	public AussomType isInterrupted(Environment env, ArrayList<AussomType> args) {
		return new AussomBool(this.isInterrupted());
	}
	
	public AussomType isAlive(Environment env, ArrayList<AussomType> args)
	{
		return new AussomBool(this.isAlive());
	}
	
	public AussomType getPriority(Environment env, ArrayList<AussomType> args) {
		return new AussomInt(this.getPriority());
	}
	
	public AussomType getName(Environment env, ArrayList<AussomType> args)
	{
		return new AussomString(this.getName());
	}
	
	public AussomType join(Environment env, ArrayList<AussomType> args) {
		long mills = ((AussomInt)args.get(0)).getValue();
		try {
			this.join(mills);
		} catch(IllegalArgumentException iae) {
			return new AussomException("thread.join(): Illegal argument of mills provided, cannot be negative.");
		} catch (InterruptedException e) {
			return new AussomException("thread.join(): Interrupted exception. " + e.getMessage());
		}
		return env.getClassInstance();
	}
	
	public AussomType isDaemon(Environment env, ArrayList<AussomType> args)
	{
		return new AussomBool(this.isDaemon());
	}
	
	public AussomType toString(Environment env, ArrayList<AussomType> args)
	{
		return new AussomString(this.toString());
	}
	
	public AussomType getId(Environment env, ArrayList<AussomType> args)
	{
		return new AussomInt(this.getId());
	}
	
	public AussomType getThreadState(Environment env, ArrayList<AussomType> args) {
		Thread.State ts = this.getState();
		switch(ts) {
			case NEW:
				return new AussomString("st_new");
			case RUNNABLE:
				return new AussomString("st_runnable");
			case BLOCKED:
				return new AussomString("st_blocked");
			case WAITING:
				return new AussomString("st_waiting");
			case TIMED_WAITING:
				return new AussomString("st_timed_waiting");
			case TERMINATED:
				return new AussomString("st_terminated");
			default:
				return new AussomException("thread.getThreadState(): Internal error. Unknown thread state '" + ts.name() + "' found.");
		}
	}
	
	public AussomType getOnRun(Environment env, ArrayList<AussomType> args) {
		if(this.ac == null) return new AussomNull();
		else return ac;
	}
	
	/*
	 * Actually run the thing.
	 * @see java.lang.Thread#run()
	 */
	public void run() {
		AussomList args = new AussomList();
		
		/*
		 * For thead we need to clone the environment because cross thread
		 * calls to environment aren't allowed. 
		 */
		Environment env = ac.getEnv().clone(ac.getEnv().getCurObj());
		AussomType ret = ac.call(env, args);
		if (ret.isEx()) {
			console.get().log(((AussomException)ret).stackTraceToString());
		}
	}
	
	public AussomType aussomFunction(Environment env, ArrayList<AussomType> args) throws aussomException {
		String variable = ((AussomString)args.get(0)).getValueString();
		return new AussomNull();
	}
}
