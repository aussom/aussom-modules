package com.lehman.aussom;

import com.aussom.Environment;
import com.aussom.types.*;

import java.util.ArrayList;
import java.util.Timer;

public class AussomTimer extends Timer {
    private AussomCallback onTimeOut = null;
	private long timeoutMills = 0;
	public boolean running = false;
	public boolean isInterval = false;
	
	private AussomTimerTask ttask = null;
	
	public AussomTimer() { super(true); }
	
	public AussomType newTimer(Environment env, ArrayList<AussomType> args) {
		this.timeoutMills = ((AussomInt)args.get(0)).getValue();
		this.onTimeOut = (AussomCallback)args.get(1);
		this.ttask = new AussomTimerTask(this.onTimeOut, this);
		return new AussomNull();
	}
	
	public void start() {
		this.schedule(this.ttask, this.timeoutMills);
		this.running = true;
	}
	
	public AussomType start(Environment env, ArrayList<AussomType> args) {
		this.start();
		return env.getClassInstance();
	}
	
	public void startInterval() throws IllegalStateException {
		this.scheduleAtFixedRate(this.ttask, 0, this.timeoutMills);
		this.isInterval = true;
		this.running = true;
	}
	
	public AussomType startInterval(Environment env, ArrayList<AussomType> args) {
		try {
			this.startInterval();
		} catch(IllegalStateException ex) {
			return new AussomException("timer.startInterval(): " + ex.getMessage());
		}
		return env.getClassInstance();
	}
	
	public void stop() {
		if(this.ttask != null) this.cancel();
		this.running = false;
	}
	
	public AussomType stop(Environment env, ArrayList<AussomType> args) {
		this.stop();
		return env.getClassInstance();
	}
	
	public void restart() throws Exception {
		if(this.ttask != null) {
			this.ttask.cancel();
			this.running = false;
			this.ttask = new AussomTimerTask(this.onTimeOut, this);
			this.schedule(this.ttask, this.timeoutMills);
			this.purge();
			this.running = true;
		}
	}
	
	public AussomType restart(Environment env, ArrayList<AussomType> args) {
		try {
			this.restart();
		} catch(Exception e) {
			return new AussomException("timer.restart(): Unhandled exception. " + e.getMessage());
		}
		return env.getClassInstance();
	}
	
	/* Getters */
	public long getDurMills() { return this.timeoutMills; }
	public boolean isRunning()
	{
		return this.running;
	}
	
	public AussomType getDuration(Environment env, ArrayList<AussomType> args)
	{
		return new AussomInt(this.timeoutMills);
	}
	
	public void setDurMills(long Mills) { this.timeoutMills = Mills; }
	
	/* Setters */
	public AussomType setDuration(Environment env, ArrayList<AussomType> args) {
		this.timeoutMills = ((AussomInt)args.get(0)).getValue();
		return env.getClassInstance();
	}
}
