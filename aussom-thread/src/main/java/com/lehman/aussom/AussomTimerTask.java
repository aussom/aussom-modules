package com.lehman.aussom;

import com.aussom.Environment;
import com.aussom.types.AussomCallback;
import com.aussom.types.AussomList;
import com.aussom.types.AussomObject;
import com.aussom.types.AussomType;

import java.util.TimerTask;

public class AussomTimerTask extends TimerTask {
    private AussomTimer tmr = null;
	private AussomCallback onTimeOut = null;

	public AussomTimerTask(AussomCallback OnTimeOut, AussomTimer Tmr) { this.tmr = Tmr; this.onTimeOut = OnTimeOut; }

	@Override
	public void run() {
		if(this.onTimeOut != null) {
			AussomObject obj = this.onTimeOut.getObj();
			/*
			 * For thead we need to clone the environment because cross thread
			 * calls to environment aren't allowed.
			 */
			Environment env = this.onTimeOut.getEnv().clone(this.onTimeOut.getEnv().getCurObj());
			AussomType ret = this.onTimeOut.call(env, new AussomList());
			if(!tmr.isInterval) tmr.running = false;
		}
	}
}
