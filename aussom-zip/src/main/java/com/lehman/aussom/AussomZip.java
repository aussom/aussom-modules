/*
 * Copyright 2024 Austin Lehman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.lehman.aussom;

import com.aussom.Engine;
import com.aussom.Environment;
import com.aussom.ast.astClass;
import com.aussom.ast.astNodeType;
import com.aussom.ast.aussomException;
import com.aussom.stdlib.ABuffer;
import com.aussom.types.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import java.util.ArrayList;

public class AussomZip {
	private AussomZipFormat format = AussomZipFormat.GZIP;

	// Byte Streams
	private ByteArrayOutputStream baos = null;
	private ByteArrayInputStream bais = null;

	// Zip Streams
	private ZipOutputStream zos = null;
	private ZipInputStream zis = null;

	public AussomZip() { }

	public AussomType newZip(Environment env, ArrayList<AussomType> args) {
		return new AussomNull();
	}

	public void addEntry(String fileNameInZip, byte[] buff) throws aussomException {
		if(this.baos == null) {
			this.baos = new ByteArrayOutputStream();
			this.zos = new ZipOutputStream(baos);
		}

		try {
			ZipEntry ze = new ZipEntry(fileNameInZip);
			this.zos.putNextEntry(ze);
			if(buff != null)
				this.zos.write(buff);
			this.zos.closeEntry();
		} catch (IOException e) {
			throw new aussomException("zip.addEntry(): IO Exception. " + e.getMessage());
		}
	}

	public AussomType addEntry(Environment env, ArrayList<AussomType> args) {
		String fileNameInZip = ((AussomString)args.get(0)).getValueString();
		if(args.get(1).getType() == cType.cObject) {
			AussomObject obj = (AussomObject)args.get(1);
			if((obj.getExternObject() != null)&&(obj.getExternObject() instanceof ABuffer)) {
				ABuffer ab = (ABuffer)obj.getExternObject();
				try {
					this.addEntry(fileNameInZip, ab.getBuffer());
				} catch (aussomException e) {
					return new AussomException(e.getMessage());
				}
			} else
				return new AussomException("zip.addEntry(): External object is null or not of type ABuffer.");

		} else if(args.get(1).isNull()) {
			if(fileNameInZip.endsWith("/")) {
				try {
					this.addEntry(fileNameInZip, null);
				} catch (aussomException e) {
					return new AussomException(e.getMessage());
				}
			} else
				return new AussomException("zip.addEntry(): Attempt to add entry with data null, so expecting folder but supplied file name doesn't end in '/' character.");
		} else
			return new AussomException("zip.addEntry(): Expecting second argument to be a buffer object or null.");

		return env.getClassInstance();
	}

	public void setBuffer(byte[] data) throws aussomException {
		if(this.bais != null) {
			try {
				this.bais.close();
			} catch (IOException e) {
				throw new aussomException("zip.setBuffer(): IO exception. Failed while closing byte array input stream. " + e.getMessage());
			}
		}
		this.bais = new ByteArrayInputStream(data);
	}

	public AussomType setBuffer(Environment env, ArrayList<AussomType> args) {
		if(args.get(0).getType() == cType.cObject) {
			AussomObject obj = (AussomObject)args.get(0);
			if((obj.getExternObject() != null)&&(obj.getExternObject() instanceof ABuffer)) {
				ABuffer ab = (ABuffer)obj.getExternObject();
				try {
					this.setBuffer(ab.getBuffer());
				} catch (aussomException e) {
					return new AussomException(e.getMessage());
				}
			} else
				return new AussomException("zip.setBuffer(): External object is null or not of type ABuffer.");

		} else
			return new AussomException("zip.setBuffer(): Expecting argument to be of type object, found '" + args.get(0).getType().name() + "' instead.");

		return env.getClassInstance();
	}

	public AussomType getBuffer(Environment env, ArrayList<AussomType> args) {
		Engine eng = env.getEngine();
		if(eng.getClasses().containsKey("buffer")) {
			astClass cls = eng.getClassByName("buffer");
			try {
				AussomList bargs = new AussomList();
				args.add(new AussomInt(1024));
				AussomObject ao = (AussomObject) cls.instantiate(env, false, bargs);
				ABuffer cb = (ABuffer) ao.getExternObject();
				cb.setBuffer(this.getBuffer());
				return ao;
			} catch (aussomException e) {
				return new AussomException(e.getMessage());
			}
		} else {
			return new AussomException("zip.getBuffer(): Class 'buffer' not found.");
		}
	}

	public byte[] getBuffer() throws aussomException {
		try {
			if(this.baos != null) {
				this.zos.finish();
				byte[] buff = this.baos.toByteArray();
				return buff;
			} else
				throw new aussomException("zip.getBuffer(): Zip object has no data.");
		} catch (IOException e) {
			throw new aussomException("zip.getBuffer(): IO exception. " + e.getMessage());
		}
	}

	public AussomType getEntries(Environment env, ArrayList<AussomType> args) {
		AussomList al = new AussomList();

		if(this.bais != null) {
			this.zis = new ZipInputStream(this.bais);

			try {
				ZipEntry ze = null;
				while ((ze = this.zis.getNextEntry()) != null) {
				    String zFileName = ze.getName();
				    al.add(new AussomString(zFileName));
				}

				this.zis.close();
			} catch (IOException e) {
				return new AussomException("zip.getEntries(): IO exception. " + e.getMessage());
			}
			this.bais.reset();
		} else
			return new AussomException("zip.getEntries(): There is no data in zip object.");

		return al;
	}

	public byte[] getEntry(String FileName) throws aussomException {
		byte[] edata = new byte[0];

		this.zis = new ZipInputStream(this.bais);

		try {
			ZipEntry ze = null;
			while ((ze = this.zis.getNextEntry()) != null) {
			    String zFileName = ze.getName();
			    if(zFileName.equals(FileName)) {
			    	ByteArrayOutputStream dataOut = new ByteArrayOutputStream();

			    	byte[] buff = new byte[1024];
			    	int read = -1;
			    	while((read = this.zis.read(buff)) != -1) {
			    		dataOut.write(buff, 0, read);
			    	}
			    	edata = dataOut.toByteArray();
			    	dataOut.close();
			    }
			}

			this.zis.close();
			this.bais.reset();

			return edata;
		} catch (IOException e) {
			throw new aussomException("zip.getEntry(): IO exception. " + e.getMessage());
		}
	}

	public AussomType getEntry(Environment env, ArrayList<AussomType> args) {
		String fname = ((AussomString) args.get(0)).getValueString();

		Engine eng = env.getEngine();
		if(eng.getClasses().containsKey("buffer")) {
			astClass cls = eng.getClassByName("buffer");
			try {
				AussomList bargs = new AussomList();
				args.add(new AussomInt(1024));
				AussomObject ao = (AussomObject) cls.instantiate(env, false, bargs);
				ABuffer cb = (ABuffer) ao.getExternObject();
				cb.setBuffer(this.getEntry(fname));
				return ao;
			} catch (aussomException e) {
				return new AussomException(e.getMessage());
			}
		} else {
			return new AussomException("zip.getEntry(): Class 'buffer' not found.");
		}
	}
}
