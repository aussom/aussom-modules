/*
 * Copyright 2024 Austin Lehman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

static class app {
    public loadJar(string jarName) {
        c.log("This is the point where the app class will dynamically load '" + jarName + "'.");
    }
}

include aunit;
include zip;

class ut_zip : test
{
	private zipObj = null;
	private textStr = "This text will be saved into the buffer object in order to be compressed.";
	private fbuff = null;

	public main(args)
	{
		// Instantiate our test and then run it.
		tt = new ut_zip();
		tt.run();
		if (tt.getResults().failed > 0) {
		    throw ("One or more tests failed!");
		}
	}

	/*
	 * Test constructor. We set the test name here and do
	 * any initialization we need for the test here.
	 */
	public ut_zip()
	{
		this.setName("Aussom zip.aus");

		// element tests
		this
			.add("(zip) Instantiate zip object.", ::zipInstantiate)
			.add("(zip) Add text to zip object.", ::addToZip)
			.add("(zip) Get entries from zip object.", ::getEntries)
			.add("(zip) Get entry data from zip object.", ::getEntry)
			.add("(zip) Write zip file to disk.", ::writeZipFile)
			.add("(zip) Read zip file from disk.", ::readZipFile)
		;
	}

	/**********************************************************************
	 * zip tests
	 *********************************************************************/
	public zipInstantiate() {
		this.zipObj = new zip();
		return this.expectObject(this.zipObj, "zip");
	}

	public addToZip() {
		buff = new buffer();
		buff.setString(this.textStr);
		this.zipObj.addEntry("text_entry.data", buff);
		this.zipObj.addEntry("new_folder/");
		this.zipObj.addEntry("new_folder/text.data", buff);
		return this.expect(true, true);
	}

	public getEntries() {
		zipped = this.zipObj.getBuffer();		// Get the compressed data
		this.zipObj.setBuffer(zipped);			// Set the compressed data
		entries = this.zipObj.getEntries();
		return this.expect(entries[0], "text_entry.data");
	}

	public getEntry() {
		buff = this.zipObj.getEntry("text_entry.data");
		str = buff.getString();
		return this.expectObject(buff, "buffer");
	}

	public writeZipFile() {
		this.fbuff = this.zipObj.getBuffer();
		return this.expect(this.fbuff != null, true);
	}

	public readZipFile() {
		this.zipObj.setBuffer(this.fbuff);
		unzipped = this.zipObj.getEntry("text_entry.data");
		str = unzipped.getString();
		return this.expect(str, this.textStr);
	}
}
