/*
 * Copyright 2024 Austin Lehman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.lehman.aussom;

import com.aussom.Engine;
import com.aussom.Environment;
import com.aussom.ast.astClass;
import com.aussom.ast.aussomException;
import com.aussom.stdlib.ABuffer;
import com.aussom.types.*;

import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;

public class AussomUdpPacket extends ABuffer {
    private DatagramPacket pack = null;

    public AussomUdpPacket() { }

    public DatagramPacket getPacket() { return this.pack; }

	public AussomType newUdpPacket(Environment env, ArrayList<AussomType> args) {
		int length = (int)((AussomInt)args.get(0)).getValue();
		super.newBuffer(length);
		this.pack = new DatagramPacket(this.buff, length);

		return new AussomNull();
	}

	public AussomType getAddress(Environment env, ArrayList<AussomType> args) {
		String thost = this.pack.getAddress().toString();
		if(thost.startsWith("/")) thost = thost.replace("/", "");
		return new AussomString(thost);
	}

	public AussomType getData(Environment env, ArrayList<AussomType> args) {
		Engine eng = env.getEngine();
		if(eng.getClasses().containsKey("buffer")) {
			astClass cls = eng.getClassByName("buffer");
			try {
				AussomList bargs = new AussomList();
                args.add(new AussomInt(1024));
                AussomObject ao = (AussomObject) cls.instantiate(env, false, bargs);
				ao.setExternObject(this);
				return ao;
			} catch (aussomException e) {
				return new AussomException("updPacket.getData(): Class 'buffer'.");
			}
		} else
			return new AussomException("udpPacket.getData(): Class 'buffer' not found.");
	}

	public AussomType getLength(Environment env, ArrayList<AussomType> args) {
		return new AussomInt(this.pack.getLength());
	}

	public AussomType getOffset(Environment env, ArrayList<AussomType> args) {
		return new AussomInt(this.pack.getOffset());
	}

	public AussomType getPort(Environment env, ArrayList<AussomType> args) {
		return new AussomInt(this.pack.getPort());
	}

	public AussomType getSocketAddress(Environment env, ArrayList<AussomType> args) {
		String thost = this.pack.getSocketAddress().toString();
		if(thost.startsWith("/")) thost = thost.replace("/", "");
		return new AussomString(thost);
	}

	public AussomType setAddress(Environment env, ArrayList<AussomType> args) {
		try {
			this.pack.setAddress(InetAddress.getByName(((AussomString)args.get(0)).getValueString()));
		} catch (UnknownHostException e) {
			return new AussomException("udpPacket.setAddress(): Unknown host exception.");
		}
		return env.getClassInstance();
	}

	public AussomType setData(Environment env, ArrayList<AussomType> args) {
		AussomObject obj = (AussomObject)args.get(0);

		if(obj.getExternObject() != null) {
			if(obj.getExternObject() instanceof ABuffer) {
				this.buff = ((ABuffer)obj.getExternObject()).getBuffer();
				return env.getClassInstance();
			} else
				return new AussomException("udpPacket.setData(): Object sent has external object of type '" + obj.getExternObject().getClass().getName() + "', expecting 'ABuffer'.");
		} else
			return new AussomException("udpPacket.setData(): Object sent has no external object.");
	}

	public AussomType setLength(Environment env, ArrayList<AussomType> args) {
		this.pack.setLength((int)((AussomInt)args.get(0)).getValue());
		return env.getClassInstance();
	}

	public AussomType setPort(Environment env, ArrayList<AussomType> args) {
		this.pack.setPort((int)((AussomInt)args.get(0)).getValue());
		return env.getClassInstance();
	}

	public AussomType setSocketAddress(Environment env, ArrayList<AussomType> args) {
		this.pack.setSocketAddress(new InetSocketAddress(((AussomString)args.get(0)).getValueString(), ((int)((AussomInt)args.get(1)).getValue())));
		return env.getClassInstance();
	}

	public AussomType setString(Environment env, ArrayList<AussomType> args) throws aussomException {
        this.setPacketString(env, args);
        return env.getClassInstance();
	}

	public AussomType setPacketString(Environment env, ArrayList<AussomType> args) throws aussomException {
        super.setString(env, args);
		this.pack.setData(this.buff);
		return new AussomNull();
	}
}
