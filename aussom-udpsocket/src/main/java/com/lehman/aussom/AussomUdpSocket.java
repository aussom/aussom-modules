/*
 * Copyright 2024 Austin Lehman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.lehman.aussom;

import com.aussom.Environment;
import com.aussom.ast.aussomException;
import com.aussom.types.*;

import java.io.IOException;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.util.ArrayList;

public class AussomUdpSocket {
	protected DatagramSocket sock = null;

	protected String host = "localhost";
	protected Integer port = 4444;

	public AussomUdpSocket() {  }

	public AussomType aussomFunction(Environment env, ArrayList<AussomType> args) throws aussomException {
		String variable = ((AussomString)args.get(0)).getValueString();
		return new AussomNull();
	}
	
	public AussomType newUdpSocket(Environment env, ArrayList<AussomType> args) {
		this.host = null;
		this.port = null;
		
		if((!args.get(0).isNull())&&(!args.get(1).isNull())) {
			this.host = ((AussomString)args.get(0)).getValueString();
			this.port = (int)((AussomInt)args.get(1)).getValue();
		}
		
		try {
			if((this.host == null)||(this.port == null))
				this.sock = new DatagramSocket();
			else {
				this.sock = new DatagramSocket(new InetSocketAddress(this.host, this.port));
			}
		} catch (IOException e) {
			return new AussomException("udpSocket.newUdpSocket(): Connection attempt, IO exception. (" + e.getMessage() + ")");
		}
		
		return new AussomNull();
	}
	
	public AussomType bind(Environment env, ArrayList<AussomType> args) {
		this.host = ((AussomString)args.get(0)).getValueString();
		this.port = (int)((AussomInt)args.get(1)).getValue();
		
		if(this.host.equals(""))
			this.host = "localhost";
		
		try {
			this.sock.bind(new InetSocketAddress(this.host, this.port));
		} catch (SocketException e) {
			return new AussomException("udpSocket.bind(): Socket exception. (" + e.getMessage() + ")");
		}
		
		return env.getClassInstance();
	}
	
	public AussomType close(Environment env, ArrayList<AussomType> args) {
		this.sock.close();
		return env.getClassInstance();
	}
	
	public AussomType connect(Environment env, ArrayList<AussomType> args) {
		this.host = ((AussomString)args.get(0)).getValueString();
		this.port = (int)((AussomInt)args.get(1)).getValue();
		
		if(this.host.equals(""))
			this.host = "localhost";
		
		try {
			this.sock.connect(new InetSocketAddress(this.host, this.port));
		} catch (SocketException e) {
			return new AussomException("udpSocket.bind(): Socket exception. (" + e.getMessage() + ")");
		}
		
		return env.getClassInstance();
	}
	
	public AussomType disconnect(Environment env, ArrayList<AussomType> args) {
		this.sock.disconnect();
		return env.getClassInstance();
	}
	
	public AussomType getBroadcast(Environment env, ArrayList<AussomType> args) {
		try {
			return new AussomBool(this.sock.getBroadcast());
		} catch (SocketException e) {
			return new AussomException("udpSocket.getBroadcast(): Socket exception. (" + e.getMessage() + ")");
		}
	}
	
	public AussomType getInetAddress(Environment env, ArrayList<AussomType> args) {
		return new AussomString(this.sock.getInetAddress().toString());
	}
	
	public AussomType getLocalAddress(Environment env, ArrayList<AussomType> args) {
		return new AussomString(this.sock.getLocalAddress().toString());
	}
	
	public AussomType getLocalSocketAddress(Environment env, ArrayList<AussomType> args) {
		return new AussomString(this.sock.getLocalSocketAddress().toString());
	}
	
	public AussomType getPort(Environment env, ArrayList<AussomType> args) {
		return new AussomInt(this.sock.getPort());
	}
	
	public AussomType getLocalPort(Environment env, ArrayList<AussomType> args) {
		return new AussomInt(this.sock.getLocalPort());
	}
	
	public AussomType getRxBufferSize(Environment env, ArrayList<AussomType> args) {
		try {
			return new AussomInt(this.sock.getReceiveBufferSize());
		} catch (SocketException e) {
			return new AussomException("udpSocket.getRxBufferSize(): Socket exception. (" + e.getMessage() + ")");
		}
	}
	
	public AussomType getTxBufferSize(Environment env, ArrayList<AussomType> args) {
		try {
			return new AussomInt(this.sock.getSendBufferSize());
		} catch (SocketException e) {
			return new AussomException("udpSocket.getTxBufferSize(): Socket exception. (" + e.getMessage() + ")");
		}
	}
	
	public AussomType getSoTimeout(Environment env, ArrayList<AussomType> args) {
		try {
			return new AussomInt(this.sock.getSoTimeout());
		} catch (SocketException e) {
			return new AussomException("udpSocket.getSoTimeout(): Socket exception. (" + e.getMessage() + ")");
		}
	}
	
	public AussomType getTrafficClass(Environment env, ArrayList<AussomType> args) {
		try {
			return new AussomInt(this.sock.getTrafficClass());
		} catch (SocketException e) {
			return new AussomException("udpSocket.getTrafficClass(): Socket exception. (" + e.getMessage() + ")");
		}
	}
	
	public AussomType isBound(Environment env, ArrayList<AussomType> args) {
		return new AussomBool(this.sock.isBound());
	}
	
	public AussomType isClosed(Environment env, ArrayList<AussomType> args) {
		return new AussomBool(this.sock.isClosed());
	}
	
	public AussomType isConnected(Environment env, ArrayList<AussomType> args) {
		return new AussomBool(this.sock.isConnected());
	}
	
	public AussomType setBroadcast(Environment env, ArrayList<AussomType> args) {
		try {
			this.sock.setBroadcast(((AussomBool)args.get(0)).getValue());
			return env.getClassInstance();
		} catch (SocketException e) {
			return new AussomException("udpSocket.setBroadcast(): Socket exception. (" + e.getMessage() + ")");
		}
	}
	
	public AussomType setRxBufferSize(Environment env, ArrayList<AussomType> args) {
		try {
			this.sock.setReceiveBufferSize((int)((AussomInt)args.get(0)).getValue());
			return env.getClassInstance();
		} catch (SocketException e) {
			return new AussomException("udpSocket.setRxBufferSize(): Socket exception. (" + e.getMessage() + ")");
		}
	}
	
	public AussomType setTxBufferSize(Environment env, ArrayList<AussomType> args) {
		try {
			this.sock.setSendBufferSize((int)((AussomInt)args.get(0)).getValue());
			return env.getClassInstance();
		} catch (SocketException e) {
			return new AussomException("udpSocket.setTxBufferSize(): Socket exception. (" + e.getMessage() + ")");
		}
	}
	
	public AussomType setReuseAddress(Environment env, ArrayList<AussomType> args) {
		try {
			this.sock.setReuseAddress(((AussomBool)args.get(0)).getValue());
			return env.getClassInstance();
		} catch (SocketException e) {
			return new AussomException("udpSocket.setReuseAddress(): Socket exception. (" + e.getMessage() + ")");
		}
	}
	
	public AussomType setSoTimeout(Environment env, ArrayList<AussomType> args) {
		try {
			this.sock.setSoTimeout((int)((AussomInt)args.get(0)).getValue());
			return env.getClassInstance();
		} catch (SocketException e) {
			return new AussomException("udpSocket.setSoTimeout(): Socket exception. (" + e.getMessage() + ")");
		}
	}
	
	public AussomType setTrafficClass(Environment env, ArrayList<AussomType> args) {
		try {
			this.sock.setTrafficClass((int)((AussomInt)args.get(0)).getValue());
			return env.getClassInstance();
		} catch (SocketException e) {
			return new AussomException("udpSocket.setTrafficClass(): Socket exception. (" + e.getMessage() + ")");
		}
	}
	
	public AussomType send(Environment env, ArrayList<AussomType> args) {
		try {
			AussomObject obj = (AussomObject)args.get(0);
			AussomUdpPacket pkt = (AussomUdpPacket)obj.getExternObject();
            if(obj.getExternObject() != null) {
				if(obj.getExternObject() instanceof AussomUdpPacket) {
					AussomUdpPacket up = (AussomUdpPacket)obj.getExternObject();
					this.sock.send(up.getPacket());
					return env.getClassInstance();
				} else
					return new AussomException("udpSocket.send(): External object is of type '" + obj.getExternObject().getClass().getName() + "', but expecting 'udpPacket'.");
			} else
				return new AussomException("udpSocket.send(): External object is null.");
		}
		catch (SocketException e) {
			return new AussomException("udpSocket.send(): Socket exception. (" + e.getMessage() + ")");
		}
		catch (IOException e) {
			return new AussomException("udpSocket.send(): IO exception. (" + e.getMessage() + ")");
		}
	}
	
	public AussomType receive(Environment env, ArrayList<AussomType> args) {
		try {
			AussomObject obj = (AussomObject)args.get(0);
			if(obj.getExternObject() != null) {
				if(obj.getExternObject() instanceof AussomUdpPacket) {
					AussomUdpPacket up = (AussomUdpPacket)obj.getExternObject();
					this.sock.receive(up.getPacket());
					return env.getClassInstance();
				} else
					return new AussomException("udpSocket.receive(): External object is of type '" + obj.getExternObject().getClass().getName() + "', but expecting 'udpPacket'.");
			} else
				return new AussomException("udpSocket.receive(): External object is null.");
		} catch (SocketException e) {
			return new AussomException("udpSocket.receive(): Socket exception. (" + e.getMessage() + ")");
		} catch (IOException e) {
			return new AussomException("udpSocket.receive(): IO exception. (" + e.getMessage() + ")");
		}
	}
}
