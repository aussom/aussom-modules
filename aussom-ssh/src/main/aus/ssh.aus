/*
 * Copyright 2024 Austin Lehman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
include app;

/**
 * Module JAR loader class.
 */
static class ssh_module {
    public ssh_module() {
        app.loadJar("net/aussom-ssh.jar");
    }
}

/**
 * The ssh class implements various SSH functions for
 * managing a remote SSH connection.
 */
extern class ssh : com.lehman.aussom.ssh.AussomSSH {
    /**
     * Constructor creates a new ssh connection object with
     * the provided arguments.
     * @p Host is a string with the host.
     * @p UserName is a string with the user name.
     * @p Password is a string with the password.
     * @p Port is an optional int with the port number.
     * @r This object.
     */
    public ssh(string Host, string UserName, string Password, int Port = 22) {
		this.newSsh(Host, UserName, Password, Port);
		return this;
	}
	private extern newSsh(string Host, string UserName, string Password, int Port = 22);

	/**
	 * Attempts to connect to the remote host.
	 * @r This object.
	 */
	public extern connect();

	/**
	 * Disconnect from the remote host.
	 * @r This object.
	 */
	public extern disconnect();

	/**
	 * Writes the provided string over the ssh connection.
	 * @r This object.
	 */
	public extern write(string Str);

	/**
	 * Writes the provided command on the remote host.
	 * @p Command is a string with the command to write.
	 * @r This object.
	 */
	public exter writeCommand(string Command);

	/**
	 * Gets the is connected flag.
	 * @r A bool with the flag value.
	 */
	public extern getIsConnected();

	/**
	 * Gets the is channel connected flag.
	 * @r A bool with the flag value.
	 */
	public extern getIsChannelConnected();

	/**
	 * Reads a single byte from the remote system
	 * and returns it as a string.
	 * @r A string with the read byte.
	 */
	public extern read();

	/**
	 * Reads until the provided string is found.
	 * @p UntilStr is a string to match.
	 * @r A string with the bytes read.
	 */
	public extern readUntil(string UntilStr);
}