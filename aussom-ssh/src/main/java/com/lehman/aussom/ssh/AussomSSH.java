/*
 * Copyright 2024 Austin Lehman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.lehman.aussom.ssh;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import com.aussom.Environment;
import com.aussom.ast.aussomException;
import com.aussom.types.*;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

public class AussomSSH {
    private JSch con = null;
	private Session sess = null;
	private Channel chan = null;

	private String host = "localhost";
	private String userName = "";
	private String password = "";
	private int port = 22;

	private int connectTimeoutMills = 30000;

	private InputStream is = null;
	private BufferedReader br = null;
	private OutputStream os = null;

	public AussomSSH() { this.con = new JSch(); }

	public AussomType newSsh(Environment env, ArrayList<AussomType> args) throws aussomException {
		this.host = ((AussomString)args.get(0)).getValueString();
		this.userName = ((AussomString)args.get(1)).getValueString();
		this.password = ((AussomString)args.get(2)).getValueString();
		this.port = (int)((AussomInt)args.get(3)).getValue();
		return new AussomNull();
	}

	/* Chained */
	public AussomType connect(Environment env, ArrayList<AussomType> args) {
		try {
			this.sess = this.con.getSession(this.userName, this.host, this.port);
			this.sess.setPassword(this.password);

			// TODO: Add option to accept checking.
			this.sess.setConfig("StrictHostKeyChecking", "no");
			this.sess.connect(this.connectTimeoutMills);
			this.chan = this.sess.openChannel("shell");

			this.is = this.chan.getInputStream();
			this.os = this.chan.getOutputStream();

			this.chan.connect(this.connectTimeoutMills);
		} catch (JSchException e) {
			return new AussomException("Failed to establish SSH connection. " + e.getMessage());
		} catch (IOException e) {
			return new AussomException("IO Exception. " + e.getMessage());
		}
		return env.getClassInstance();
	}

	public AussomType disconnect(Environment env, ArrayList<AussomType> args) {
		if(this.chan != null) {
			this.chan.disconnect();
			this.chan = null;
		}

		if(this.sess != null) {
			this.sess.disconnect();
			this.sess = null;
		}

		if(this.is != null) {
			try{ this.is.close(); }
			catch (IOException e) { }
			this.is = null;
		}

		if(this.os != null) {
			try{ this.os.close(); }
			catch (IOException e) { }
			this.os = null;
		}

		return env.getClassInstance();
	}

	public AussomType write(Environment env, ArrayList<AussomType> args) {
		String str = ((AussomString)args.get(0)).getValueString();
		if(this.os != null) {
			try {
				this.os.write(str.getBytes());
				this.os.flush();
			} catch (IOException e) {
				return new AussomException("IO Exception. " + e.getMessage());
			}

		} else
			return new AussomException("Channel output stream is null.");

		return env.getClassInstance();
	}

	public AussomType writeCommand(Environment env, ArrayList<AussomType> args) {
		String str = ((AussomString)args.get(0)).getValueString() + "\n";
		if(this.os != null) {
			try {
				this.os.write(str.getBytes());
				this.os.flush();
			} catch (IOException e) {
				return new AussomException("IO Exception. " + e.getMessage());
			}

		} else
			return new AussomException("Channel output stream is null.");

		return env.getClassInstance();
	}

	/* Not chained */
	public AussomType getIsConnected(Environment env, ArrayList<AussomType> args) {
		if(this.sess != null) {
			if(this.sess.isConnected()) { return new AussomBool(true); }
		}
		return new AussomBool(false);
	}

	public AussomType getIsChannelConnected(Environment env, ArrayList<AussomType> args) {
		if(this.chan != null) {
			if(this.chan.isConnected()) { return new AussomBool(true); }
		}
		return new AussomBool(false);
	}

	public AussomType read(Environment env, ArrayList<AussomType> args) {
		if(this.is != null) {
			try {
				byte b[] = new byte[1];
				this.is.read(b);
				return new AussomString(new String(b));
			} catch (IOException e) {
				return new AussomException("IO Exception. " + e.getMessage());
			}
		} else
			return new AussomException("Channel input stream is null.");
	}

	public AussomType readUntil(Environment env, ArrayList<AussomType> args) {
		String until = ((AussomString)args.get(0)).getValueString();
		String ret = "";
		if(this.is != null) {
			try {
				byte b[] = new byte[1];
				while(true) {
					this.is.read(b);
					ret += new String(b);
					if(ret.endsWith(until))
						return new AussomString(ret);
				}
			} catch (IOException e) {
				return new AussomException("IO Exception. " + e.getMessage());
			}
		} else
			return new AussomException("Channel input stream is null.");
	}
}
