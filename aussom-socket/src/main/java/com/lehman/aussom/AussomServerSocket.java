package com.lehman.aussom;

import com.aussom.Engine;
import com.aussom.Environment;
import com.aussom.ast.astClass;
import com.aussom.stdlib.console;
import com.aussom.types.*;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.SocketException;
import java.util.ArrayList;

public class AussomServerSocket {
    protected ServerSocket sock = null;
	protected String host = "localhost";
	protected int port = 80;
	protected boolean connected = false;

	/* Options */
	protected Integer soTimeout = null;

	public AussomServerSocket() { }

	public AussomType newServerSocket(Environment env, ArrayList<AussomType> args) {
		this.host = "localhost";
		this.port = (int)((AussomInt)args.get(0)).getValue();
		try {
			this.sock = new ServerSocket(port);
			if(this.soTimeout != null) this.sock.setSoTimeout(this.soTimeout);
			this.connected = true;
		} catch (IOException e) {
			return new AussomException("serverSocket.newServerSocket(): IO Exception. (" + e.getMessage() + ")");
		}
		return new AussomNull();
	}

	public AussomType bind(Environment env, ArrayList<AussomType> args) {
		this.host = ((AussomString)args.get(0)).getValueString();
		try {
			this.sock.bind(new InetSocketAddress(this.host, this.port));
		} catch (IOException e) {
			return new AussomException("serverSocket.bind(): IO Exception. (" + e.getMessage() + ")");
		}
		return env.getClassInstance();
	}

	public AussomType close(Environment env, ArrayList<AussomType> args) {
		if(this.connected) {
			try {
				this.sock.close();
			} catch (IOException e) {
				return new AussomException("serverSocket.close(): IO Exception. (" + e.getMessage() + ")");
			}
			this.connected = false;
		}
		return env.getClassInstance();
	}

	public AussomType accept(Environment env, ArrayList<AussomType> args) {
		Engine eng = env.getEngine();
		if(eng.getClasses().containsKey("socket")) {
			astClass cls = eng.getClassByName("socket");
			try {
				AussomList sargs = new AussomList();
				sargs.add(new AussomString("localhost"));
				sargs.add(new AussomInt(80));
				AussomObject ao = (AussomObject) cls.instantiate(env, false, sargs);
				if (ao instanceof AussomException) {
					console.get().log(((AussomException) ao).stackTraceToString());
				} else {
					AussomSocket tsock = (AussomSocket)ao.getExternObject();
					tsock.setSocket(this.sock.accept());
				}
				return ao;
			} catch (Exception e) {
				return new AussomException("serverSocket.accept(): Class 'socket'.");
			}
        } else {
			return new AussomException("serverSocket.accept(): Class 'socket' not found.");
		}
	}

	public AussomType getHost(Environment env, ArrayList<AussomType> args) {
		return new AussomString(this.host);
	}

	public AussomType getPort(Environment env, ArrayList<AussomType> args) {
		return new AussomInt(this.port);
	}

	public AussomType setReuseAddress(Environment env, ArrayList<AussomType> args) {
		try {
			this.sock.setReuseAddress(((AussomBool)args.get(0)).getValue());
		} catch (SocketException e) {
			return new AussomException("serverSocket.setReuseAddress(): Socket Exception.");
		}
		return env.getClassInstance();
	}

	public AussomType getReuseAddress(Environment env, ArrayList<AussomType> args) {
		try {
			return new AussomBool(this.sock.getReuseAddress());
		} catch (SocketException e) {
			return new AussomException("serverSocket.getReuseAddress(): Socket Exception.");
		}
	}

	public AussomType isBound(Environment env, ArrayList<AussomType> args) {
		return new AussomBool(this.sock.isBound());
	}

	public AussomType isClosed(Environment env, ArrayList<AussomType> args) {
		return new AussomBool(this.sock.isClosed());
	}

	public AussomType _setPerformancePreferences(Environment env, ArrayList<AussomType> args) {
		int connectionTime = (int)((AussomInt)args.get(0)).getValue();
		int latency = (int)((AussomInt)args.get(1)).getValue();
		int bandwidth = (int)((AussomInt)args.get(2)).getValue();
		this.sock.setPerformancePreferences(connectionTime, latency, bandwidth);
		return new AussomNull();
	}

	public AussomType setReceiveBufferSize(Environment env, ArrayList<AussomType> args) {
		try {
			this.sock.setReceiveBufferSize((int)((AussomInt)args.get(0)).getValue());
		} catch (SocketException e) {
			return new AussomException("serverSocket.setReceiveBufferSize(): Socket Exception.");
		}
		return env.getClassInstance();
	}

	public AussomType getReceiveBufferSize(Environment env, ArrayList<AussomType> args) {
		try {
			return new AussomInt(this.sock.getReceiveBufferSize());
		} catch (SocketException e) {
			return new AussomException("serverSocket.getReceiveBufferSize(): Socket Exception.");
		}
	}

	public AussomType setHost(Environment env, ArrayList<AussomType> args) {
		this.host = ((AussomString)args.get(0)).getValueString();
		return env.getClassInstance();
	}

	public AussomType setPort(Environment env, ArrayList<AussomType> args) {
		this.port = (int)((AussomInt)args.get(0)).getValue();
		return env.getClassInstance();
	}

	public AussomType setSoTimeout(Environment env, ArrayList<AussomType> args) {
		this.soTimeout = (int)((AussomInt)args.get(0)).getValue();
		return env.getClassInstance();
	}

	public AussomType getSoTimeout(Environment env, ArrayList<AussomType> args) {
		return new AussomInt(this.soTimeout);
	}
}
