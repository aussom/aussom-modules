/*
 * Copyright 2024 Austin Lehman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.lehman.aussom;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;


import com.aussom.Engine;
import com.aussom.Environment;
import com.aussom.ast.astClass;
import com.aussom.ast.aussomException;
import com.aussom.stdlib.ABuffer;
import com.aussom.types.*;

public class AussomSocket {
	protected Socket sock = null;
	protected String host = "";
	protected int port = 80;
	protected boolean connected = false;

	protected BufferedReader in = null;
	protected BufferedWriter out = null;

	/* Options */
	protected Integer soTimeout = null;

	public AussomSocket() { }

	public void setSocket(Socket Sock) {
		this.sock = Sock;
		if(!this.sock.isClosed()) connected = true;
	}

	public AussomType newSocket(Environment env, ArrayList<AussomType> args) {
		this.host = ((AussomString)args.get(0)).getValueString();
		this.port = (int)((AussomInt)args.get(1)).getValue();

		if(!this.host.equals("")) {
			try {
				this.sock = new Socket(this.host, this.port);
				this.connected = true;
			} catch (UnknownHostException e) {
				return new AussomException("socket.newSocket(): Connection attempt, unknown host exception for host '" + this.host + "'.");
			} catch (IOException e) {
				return new AussomException("socket.newSocket(): Connection attempt, IO Exception. (" + e.getMessage() + ")");
			}
		}
		return new AussomNull();
	}

	public AussomType connect(Environment env, ArrayList<AussomType> args) {
		String thost = ((AussomString)args.get(0)).getValueString();
		if(!thost.equals(""))
			this.host = thost;

		if(connected)
			return new AussomException("socket.connect(): Socket appears to already be connected. Please close the socket before re-opening.");

		if(!this.host.equals("")) {
			try {
				this.sock = new Socket(this.host, this.port);
				if(this.soTimeout != null)
					this.sock.setSoTimeout(this.soTimeout);
				this.connected = true;
			} catch (UnknownHostException e) {
				return new AussomException("socket.connect(): Connection attempt, unknown host exception for host '" + this.host + "'.");
			} catch (IOException e) {
				return new AussomException("socket.connect(): Connection attempt, IO Exception. (" + e.getMessage() + ")");
			}
		} else {
			return new AussomException("socket.connect(): No host provided.");
		}
		return env.getClassInstance();
	}

	public AussomType bind(Environment env, ArrayList<AussomType> args) {
		try {
			this.sock.bind(new InetSocketAddress(((AussomString)args.get(0)).getValueString(), this.port));
		} catch (IOException e) {
			return new AussomException("socket.bind(): IO Exception. (" + e.getMessage() + ")");
		}
		return env.getClassInstance();
	}

	public AussomType close(Environment env, ArrayList<AussomType> args) {
		if(this.connected) {
			this.connected = false;
			try {
				this.sock.close();
			} catch (IOException e) {
				return new AussomException("socket.close(): IO Exception. (" + e.getMessage() + ")");
			}
		}
		return env.getClassInstance();
	}

	public AussomType setKeepAlive(Environment env, ArrayList<AussomType> args) {
		try {
			this.sock.setKeepAlive(((AussomBool)args.get(0)).getValue());
		} catch (SocketException e) {
			return new AussomException("socket.setKeepAlive(): Socket Exception.");
		}
		return env.getClassInstance();
	}

	public AussomType getKeepAlive(Environment env, ArrayList<AussomType> args) {
		try {
			return new AussomBool(this.sock.getKeepAlive());
		} catch (SocketException e) {
			return new AussomException("socket.getKeepAlive(): Socket Exception.");
		}
	}

	public AussomType setOOBInline(Environment env, ArrayList<AussomType> args) {
		try {
			this.sock.setOOBInline(((AussomBool)args.get(0)).getValue());
		} catch (SocketException e) {
			return new AussomException("socket.setOOBInline(): Socket Exception.");
		}
		return new AussomNull();
	}

	public AussomType getOOBInline(Environment env, ArrayList<AussomType> args) {
		try {
			return new AussomBool(this.sock.getOOBInline());
		} catch (SocketException e) {
			return new AussomException("socket.getOOBInline(): Socket Exception.");
		}
	}

	public AussomType setPerformancePreferences(Environment env, ArrayList<AussomType> args) {
		int connectionTime = (int)((AussomInt)args.get(0)).getValue();
		int latency = (int)((AussomInt)args.get(1)).getValue();
		int bandwidth = (int)((AussomInt)args.get(2)).getValue();
		this.sock.setPerformancePreferences(connectionTime, latency, bandwidth);
		return env.getClassInstance();
	}

	public AussomType setReceiveBufferSize(Environment env, ArrayList<AussomType> args) {
		try {
			this.sock.setReceiveBufferSize((int) ((AussomInt) args.get(0)).getValue());
		} catch (SocketException e) {
			return new AussomException("socket.setReceiveBufferSize(): Socket Exception.");
		}
		return env.getClassInstance();
	}

	public AussomType getReceiveBufferSize(Environment env, ArrayList<AussomType> args) {
		try {
			return new AussomInt(this.sock.getReceiveBufferSize());
		} catch (SocketException e) {
			return new AussomException("socket.getReceiveBufferSize(): Socket Exception.");
		}
	}

	public AussomType setReuseAddress(Environment env, ArrayList<AussomType> args) {
		try {
			this.sock.setReuseAddress(((AussomBool)args.get(0)).getValue());
		} catch (SocketException e) {
			return new AussomException("socket.setReuseAddress(): Socket Exception.");
		}
		return env.getClassInstance();
	}

	public AussomType getReuseAddress(Environment env, ArrayList<AussomType> args) {
		try {
			return new AussomBool(this.sock.getReuseAddress());
		} catch (SocketException e) {
			return new AussomException("socket.getReuseAddress(): Socket Exception.");
		}
	}

	public AussomType setSendBufferSize(Environment env, ArrayList<AussomType> args) {
		try {
			this.sock.setSendBufferSize((int)((AussomInt)args.get(0)).getValue());
		} catch (SocketException e) {
			return new AussomException("socket.setSendBufferSize(): Socket Exception.");
		}
		return env.getClassInstance();
	}

	public AussomType getSendBufferSize(Environment env, ArrayList<AussomType> args) {
		try {
			return new AussomInt(this.sock.getSendBufferSize());
		} catch (SocketException e) {
			return new AussomException("socket.getSendBufferSize(): Socket Exception.");
		}
	}

	public AussomType setSoLinger(Environment env, ArrayList<AussomType> args) {
		try {
			this.sock.setSoLinger(((AussomBool)args.get(0)).getValue(), (int)((AussomInt)args.get(1)).getValue());
		} catch (SocketException e) {
			return new AussomException("socket.setSoLinger(): Socket Exception.");
		}
		return env.getClassInstance();
	}

	public AussomType getSoLinger(Environment env, ArrayList<AussomType> args) {
		try {
			return new AussomInt(this.sock.getSoLinger());
		} catch (SocketException e) {
			return new AussomException("socket.getSoLinger(): Socket Exception.");
		}
	}

	public AussomType setTcpNoDelay(Environment env, ArrayList<AussomType> args) {
		try {
			this.sock.setTcpNoDelay(((AussomBool)args.get(0)).getValue());
		} catch (SocketException e) {
			return new AussomException("socket.setTcpNoDelay(): Socket Exception.");
		}
		return env.getClassInstance();
	}

	public AussomType getTcpNoDelay(Environment env, ArrayList<AussomType> args) {
		try {
			return new AussomBool(this.sock.getTcpNoDelay());
		} catch (SocketException e) {
			return new AussomException("socket.getTcpNoDelay(): Socket Exception.");
		}
	}

	public AussomType setTrafficClass(Environment env, ArrayList<AussomType> args) {
		AussomString tclass = (AussomString)args.get(0);
		int tclassVal = -1;

		if(tclass.getValueString().equals("lowcost"))
			tclassVal = 0x02;
		else if(tclass.getValueString().equals("reliability"))
			tclassVal = 0x04;
		else if(tclass.getValueString().equals("throughput"))
			tclassVal = 0x08;
		else if(tclass.getValueString().equals("lowdelay"))
			tclassVal = 0x10;
		else
			return new AussomException("socket.setTrafficClass(): Traffic class type '" + tclass.getValueString() + "' unknown.");

		try {
			this.sock.setTrafficClass(tclassVal);
		} catch (SocketException e) {
			return new AussomException("socket.setTrafficClass(): Socket Exception.");
		}
		return env.getClassInstance();
	}

	public AussomType getTrafficClass(Environment env, ArrayList<AussomType> args) {
		try {
			int tclass = this.sock.getTrafficClass();
			if(tclass == 0x02) return new AussomString("lowcost");
			else if(tclass == 0x04) return new AussomString("reliability");
			else if(tclass == 0x08) return new AussomString("throughput");
			else if(tclass == 0x10) return new AussomString("lowdelay");
		} catch (SocketException e) {
			return new AussomException("socket.getTrafficClass(): Socket Exception.");
		}
		return new AussomNull();
	}

	public AussomType readln(Environment env, ArrayList<AussomType> args) {
		if(this.connected) {
			if(this.in == null) {
				try {
					this.in = new BufferedReader(new InputStreamReader(this.sock.getInputStream()));
				} catch (IOException e) {
					return new AussomException("socket.readln(): IO Exception. (" + e.getMessage() + ")");
				}
			}

			if(this.in != null) {
				try {
					String line = in.readLine();
					if(line != null) {
						return new AussomString(line);
					} else {
						this.connected = false;
						this.sock.close();
						return new AussomException("socket.readln(): IO Exception.");
					}
				} catch (IOException e) {
					return new AussomException("socket.readln(): IO Exception. (" + e.getMessage() + ")");
				}
			}
		}
		else {
			return new AussomException("socket.readln(): Socket not connected.");
		}
		return new AussomNull();
	}

	public AussomType read(Environment env, ArrayList<AussomType> args) {
		if(this.connected) {
			if(this.in == null) {
				try {
					this.in = new BufferedReader(new InputStreamReader(this.sock.getInputStream()));
				} catch (IOException e) {
					return new AussomException("socket.read(): IO Exception. (" + e.getMessage() + ")");
				}
			}

			if(this.in != null) {
				try {
					Character c = (char)in.read();
					if(c != null) {
						return new AussomString("" + c);
					} else {
						this.connected = false;
						this.sock.close();
						return new AussomException("socket.read(): IO Exception.");
					}
				}
				catch (IOException e) {
					return new AussomException("socket.read(): IO Exception. (" + e.getMessage() + ")");
				}
			}
		}
		else {
			return new AussomException("socket.read(): Socket not connected.");
		}
		return new AussomNull();
	}

	public AussomType writeln(Environment env, ArrayList<AussomType> args) {
		if(this.connected) {
			if(this.out == null) {
				try {
					this.out = new BufferedWriter(new OutputStreamWriter(this.sock.getOutputStream()));
				} catch (IOException e) {
					return new AussomException("socket.writeln(): IO Exception. (" + e.getMessage() + ")");
				}
			}

			if(this.out != null) {
				try {
					String line = ((AussomString)args.get(0)).getValueString();
					this.out.write(line + "\n");
					this.out.flush();
				} catch (IOException e) {
					return new AussomException("socket.writeln(): IO Exception. (" + e.getMessage() + ")");
				}
			}
		}
		else {
			return new AussomException("socket.writeln(): Socket not connected.");
		}
		return env.getClassInstance();
	}

	public AussomType write(Environment env, ArrayList<AussomType> args) {
		if(this.connected) {
			if(this.in == null) {
				try {
					this.out = new BufferedWriter(new OutputStreamWriter(this.sock.getOutputStream()));
				} catch (IOException e) {
					return new AussomException("socket.write(): IO Exception. (" + e.getMessage() + ")");
				}
			}

			if(this.out != null) {
				try {
					String line = ((AussomString)args.get(0)).getValueString();
					this.out.write(line);
					this.out.flush();
				} catch (IOException e) {
					return new AussomException("socket.write(): IO Exception. (" + e.getMessage() + ")");
				}
			}
		}
		else {
			return new AussomException("socket.write(): Socket not connected.");
		}
		return env.getClassInstance();
	}

	public AussomType readBytes(Environment env, ArrayList<AussomType> args) throws aussomException {
		if(this.connected) {
			int length = (int)((AussomInt)args.get(0)).getValue();
			if(length >= 0) {
				Engine eng = env.getEngine();
				if(eng.getClasses().containsKey("buffer")) {
					astClass cls = eng.getClassByName("buffer");
					try {
						AussomList bargs = new AussomList();
						args.add(new AussomInt(1024));
						AussomObject ao = (AussomObject) cls.instantiate(env, false, bargs);
						ABuffer cb = (ABuffer) ao.getExternObject();
						byte[] buff = new byte[length];
						this.sock.getInputStream().read(buff, 0, length);
						cb.setBuffer(new byte[length]);
						return ao;
					} catch (Exception e) {
						return new AussomException("socket.readBytes(): Class 'buffer'.");
					}
                }
				else {
					return new AussomException("socket.readBytes(): Class 'buffer' not found.");
				}
			} else {
				return new AussomException("socket.readBytes(): Length cannot be less than 0.");
			}
		}
		return new AussomNull();
	}

	public AussomType readBytesTo(Environment env, ArrayList<AussomType> args) throws aussomException {
		if(this.connected) {
			ABuffer cb = null;
			byte[] buff = null;

			AussomObject obj = (AussomObject)args.get(0);
			if((obj.getExternObject() != null)&&(obj.getExternObject() instanceof ABuffer)) {
				cb = (ABuffer) obj.getExternObject();
				buff = cb.getBuffer();
			} else {
				return new AussomException("socket.readBytesTo(): External object is null or not of type buffer.");
			}

			int index = (int)((AussomInt)args.get(1)).getValue();
			int length = (int)((AussomInt)args.get(2)).getValue();

			if((index >= 0)&&(index < buff.length)) {
				if((length >= 0)&&(buff.length < index + length)) {
					try {
						this.sock.getInputStream().read(buff, 0, length);
					} catch (IOException e) {
						return new AussomException("socket.readBytes(): IO Exception. " + e.getMessage());
					}
				} else {
					return new AussomException("socket.readBytes(): Buffer overflow.");
				}
			} else {
				return new AussomException("socket.readBytes(): Index out of bounds.");
			}
		}
		return env.getClassInstance();
	}

	public AussomType writeBytes(Environment env, ArrayList<AussomType> args) {
		if(this.connected) {
			AussomObject obj = (AussomObject)args.get(0);
			if((obj.getExternObject() != null)&&(obj.getExternObject() instanceof ABuffer)) {
				ABuffer cb = (ABuffer)obj.getExternObject();
				try {
					this.sock.getOutputStream().write(cb.getBuffer());
				} catch (IOException e) {
					return new AussomException("socket.writeBytes(): IO Exception. " + e.getMessage());
				}
			} else {
				return new AussomException("socket.writeBytes(): External object is null or not of type buffer.");
			}
		}
		return env.getClassInstance();
	}

	public AussomType writeBytesFrom(Environment env, ArrayList<AussomType> args) throws aussomException {
		if(this.connected) {
			AussomObject obj = (AussomObject)args.get(0);
			int index = (int)((AussomInt)args.get(1)).getValue();
			int length = (int)((AussomInt)args.get(2)).getValue();
			if((obj.getExternObject() != null)&&(obj.getExternObject() instanceof ABuffer)) {
				ABuffer cb = (ABuffer)obj.getExternObject();
				if((index >= 0)&&(index < length)) {
					if((length >= 0) && (cb.getBuffer().length < index + length)) {
						try {
							this.sock.getOutputStream().write(cb.getBuffer(), index, length);
						} catch (IOException e) {
							return new AussomException("socket.writeBytes(): IO Exception. " + e.getMessage());
						}
					} else {
						return new AussomException("socket.writeBytes(): Buffer overflow.");
					}
				} else {
					return new AussomException("socket.writeBytes(): Index out of bounds.");
				}
			} else {
				return new AussomException("socket.writeBytes(): External object is null or not of type buffer.");
			}
		}
		return env.getClassInstance();
	}

	/*
	 * Must be called prior to connect.
	 */
	public AussomType setHost(Environment env, ArrayList<AussomType> args) {
		this.host = ((AussomString)args.get(0)).getValueString();
		return env.getClassInstance();
	}

	public AussomType setPort(Environment env, ArrayList<AussomType> args) throws aussomException {
		this.port = (int)((AussomInt)args.get(0)).getValue();
		return env.getClassInstance();
	}

	public AussomType setSoTimeout(Environment env, ArrayList<AussomType> args) throws aussomException {
		this.soTimeout = (int)((AussomInt)args.get(0)).getValue();
		return env.getClassInstance();
	}

	public AussomType getSoTimeout(Environment env, ArrayList<AussomType> args) {
		return new AussomInt(this.soTimeout);
	}

	public AussomType aussomFunction(Environment env, ArrayList<AussomType> args) throws aussomException {
		String variable = ((AussomString)args.get(0)).getValueString();
		return new AussomNull();
	}
}
