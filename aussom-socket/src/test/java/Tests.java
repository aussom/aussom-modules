/*
 * Copyright 2024 Austin Lehman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import com.aussom.Engine;
import com.aussom.Util;
import com.aussom.stdlib.Lang;
import org.junit.Assert;
import org.junit.Test;

public class Tests {

    public Tests() throws Exception {
    }

    @Test
    public void runAussomTest() throws Exception {
        Engine eng = new Engine();
        // Add the Aussom module
        Lang.get().langIncludes.put("socket.aus", Util.read("src/main/aus/socket.aus"));

        eng.loadUniverseClasses();
        eng.addIncludePath(".");
        eng.parseFile("socket-test.aus");
        int ret = eng.run();

        if (ret == 0) {
            Assert.assertTrue(true);
        } else {
            Assert.assertTrue(false);
        }
    }
}
