/*
 * Copyright 2024 Austin Lehman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.lehman.aussom;

import java.io.IOException;
import java.net.UnknownHostException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import com.aussom.Environment;
import com.aussom.types.*;

import java.util.ArrayList;

public class AussomSslSocket extends BaseSocket {
	private SSLContext sc = null;
	
	public AussomSslSocket() {  }
	
	public void setSocket(SSLSocket Sock) { this.sock = Sock; }
	
	public AussomType newSslSocket(Environment env, ArrayList<AussomType> args)
	{
		this.host = ((AussomString)args.get(0)).getValueString();
		this.port = (int)((AussomInt)args.get(1)).getValue();
		boolean allowSelfSigned = ((AussomBool)args.get(2)).getValue();
		
		if(allowSelfSigned)
			this.allowSelfSignedCerts();
		
		if(!this.host.equals(""))
		{
			try
			{
				SSLSocketFactory factory = null;
				
				if(this.sc == null)
					factory = (SSLSocketFactory) SSLSocketFactory.getDefault();
				else
					factory = (SSLSocketFactory) this.sc.getSocketFactory();
				
				this.sock = (SSLSocket) factory.createSocket(this.host, this.port);
				this.connected = true;
			}
			catch (UnknownHostException e)
			{
				return new AussomException("sslSocket.newSslSocket(): Connection attempt, unknown host exception for host '" + this.host + "'.");
			}
			catch (IOException e)
			{
				return new AussomException("sslSocket.newSslSocket(): Connection attempt, IO Exception. (" + e.getMessage() + ")");
			}
		}
		
		return new AussomNull();
	}
	
	public AussomType getCiphers(Environment env, ArrayList<AussomType> args)
	{
		AussomList suites = new AussomList();
		
		for(String s : ((SSLSocket)this.sock).getEnabledCipherSuites())
			suites.add(new AussomString(s));
		
		return suites;
	}
	
	public AussomType getProtocols(Environment env, ArrayList<AussomType> args)
	{
		AussomList protos = new AussomList();
		
		for(String s : ((SSLSocket)this.sock).getEnabledProtocols())
			protos.add(new AussomString(s));
		
		return protos;
	}
	
	public AussomType getEnableSessionCreation(Environment env, ArrayList<AussomType> args)
	{
		return new AussomBool(((SSLSocket)this.sock).getEnableSessionCreation());
	}
	
	public AussomType getNeedClientAuth(Environment env, ArrayList<AussomType> args)
	{
		return new AussomBool(((SSLSocket)this.sock).getNeedClientAuth());
	}
	
	public AussomType getUseClientMode(Environment env, ArrayList<AussomType> args)
	{
		return new AussomBool(((SSLSocket)this.sock).getUseClientMode());
	}
	
	public AussomType getWantClientAuth(Environment env, ArrayList<AussomType> args)
	{
		return new AussomBool(((SSLSocket)this.sock).getWantClientAuth());
	}
	
	public AussomType setCiphers(Environment env, ArrayList<AussomType> args)
	{
		AussomList suites = (AussomList)args.get(0);
		ArrayList<String> ss = new ArrayList<String>();
		for(AussomType n : suites.getValue())
		{
			ss.add(n.getValueString());
		}
		((SSLSocket)this.sock).setEnabledCipherSuites((String[]) ss.toArray());
		
		return env.getClassInstance();
	}
	
	public AussomType setProtocols(Environment env, ArrayList<AussomType> args)
	{
		AussomList protos = (AussomList)args.get(0);
		ArrayList<String> ps = new ArrayList<String>();
		for(AussomType n : protos.getValue())
		{
			ps.add(n.getValueString());
		}
		((SSLSocket)this.sock).setEnabledProtocols((String[]) ps.toArray());
		
		return env.getClassInstance();
	}
	
	public AussomType setEnableSessionCreation(Environment env, ArrayList<AussomType> args)
	{
		((SSLSocket)this.sock).setEnableSessionCreation(((AussomBool)args.get(0)).getValue());
		return env.getClassInstance();
	}
	
	public AussomType setNeedClientAuth(Environment env, ArrayList<AussomType> args)
	{
		((SSLSocket)this.sock).setNeedClientAuth(((AussomBool)args.get(0)).getValue());
		return env.getClassInstance();
	}
	
	public AussomType setUseClientMode(Environment env, ArrayList<AussomType> args)
	{
		((SSLSocket)this.sock).setUseClientMode(((AussomBool)args.get(0)).getValue());
		return env.getClassInstance();
	}
	
	public AussomType setWantClientAuth(Environment env, ArrayList<AussomType> args)
	{
		((SSLSocket)this.sock).setWantClientAuth(((AussomBool)args.get(0)).getValue());
		return env.getClassInstance();
	}
	
	public AussomType startHandshake(Environment env, ArrayList<AussomType> args)
	{
		try
		{
			((SSLSocket)this.sock).startHandshake();
		}
		catch (IOException e)
		{
			return new AussomException("sslSocket.startHandshake(): IO Exception. (" + e.getMessage() + ")");
		}
		return env.getClassInstance();
	}
	
	public void allowSelfSignedCerts()
	{
		TrustManager[] trustAllCerts = new TrustManager[]
		{ 
		    new X509TrustManager()
		    {     
		        public java.security.cert.X509Certificate[] getAcceptedIssuers()
		        { 
		            return new java.security.cert.X509Certificate[0];
		        } 
		        public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType)
		        {
		        	
		        } 
		        public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType)
		        {
		        	
		        }
		    } 
		}; 

		// Install the all-trusting trust manager
		try
		{
		    this.sc = SSLContext.getInstance("TLS"); 
		    this.sc.init(null, trustAllCerts, new java.security.SecureRandom());
		}
		catch (GeneralSecurityException e)
		{
			e.printStackTrace();
		} 
	}
}
