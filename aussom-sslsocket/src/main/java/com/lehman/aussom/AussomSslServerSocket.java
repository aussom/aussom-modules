/*
 * Copyright 2024 Austin Lehman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.lehman.aussom;

import com.aussom.Engine;
import com.aussom.Environment;
import com.aussom.ast.astClass;
import com.aussom.ast.aussomException;
import com.aussom.stdlib.console;
import com.aussom.types.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.ArrayList;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.TrustManagerFactory;
public class AussomSslServerSocket extends BaseServerSocket {
    public AussomSslServerSocket() { }

    public void setServerSocket(SSLServerSocket Sock) { this.sock = Sock; }

	public AussomType newSslServerSocket(Environment env, ArrayList<AussomType> args) {
		this.host = "localhost";
		this.port = (int)((AussomInt)args.get(0)).getValue();
		String certFileName = ((AussomString)args.get(1)).getValueString();
		String certPassword = ((AussomString)args.get(2)).getValueString();
		try {
			SSLServerSocketFactory factory = null;
			if(certFileName.equals(""))
				factory = (SSLServerSocketFactory) SSLServerSocketFactory.getDefault();
			else {
				try {
					factory = this.getFactoryFromCertFile(certFileName, certPassword);
				} catch (aussomException e) {
					return new AussomException(e.getMessage());
				}
			}
			this.sock = factory.createServerSocket(port);
			if(this.soTimeout != null) this.sock.setSoTimeout(this.soTimeout);
			this.connected = true;
		} catch (IOException e) {
			return new AussomException("sslServerSocket.newSslServerSocket(): IO Exception. (" + e.getMessage() + ")");
		}

		return new AussomNull();
	}

	public SSLServerSocketFactory getFactoryFromCertFile(String fileName, String password) throws aussomException {
		SSLServerSocketFactory factory = null;

		SSLContext sslContext;
		try {
			sslContext = SSLContext.getInstance( "TLS" );
			KeyManagerFactory km = KeyManagerFactory.getInstance("SunX509");
			KeyStore ks = KeyStore.getInstance("JKS");
			ks.load(new FileInputStream(fileName), password.toCharArray());
			km.init(ks, password.toCharArray());
			TrustManagerFactory tm = TrustManagerFactory.getInstance("SunX509");
			tm.init(ks);
			sslContext.init(km.getKeyManagers(), tm.getTrustManagers(), null);
			factory = sslContext.getServerSocketFactory();
		} catch (NoSuchAlgorithmException e) {
			throw new aussomException("sslServerSocket.getFactoryFromCertFile(): No such algorithm exception. (" + e.getMessage() + ")");
		} catch (KeyStoreException e) {
			throw new aussomException("sslServerSocket.getFactoryFromCertFile(): Key strore exception. (" + e.getMessage() + ")");
		} catch (CertificateException e) {
			throw new aussomException("sslServerSocket.getFactoryFromCertFile(): Certificate exception. (" + e.getMessage() + ")");
		} catch (FileNotFoundException e) {
			throw new aussomException("sslServerSocket.getFactoryFromCertFile(): File not found exception. (" + e.getMessage() + ")");
		} catch (IOException e) {
			throw new aussomException("sslServerSocket.getFactoryFromCertFile(): IO exception. (" + e.getMessage() + ")");
		} catch (UnrecoverableKeyException e) {
			throw new aussomException("sslServerSocket.getFactoryFromCertFile(): Unrecoverable key exception. (" + e.getMessage() + ")");
		} catch (KeyManagementException e) {
			throw new aussomException("sslServerSocket.getFactoryFromCertFile(): Key management exception. (" + e.getMessage() + ")");
		}

		if(factory == null) factory = (SSLServerSocketFactory) SSLServerSocketFactory.getDefault();

		return factory;
	}

	@Override
	public AussomType accept(Environment env, ArrayList<AussomType> args) {
		Engine eng = env.getEngine();
		if(eng.getClasses().containsKey("sslSocket")) {
			astClass cls = eng.getClassByName("sslSocket");
			try {
				AussomList sargs = new AussomList();
				sargs.add(new AussomString("localhost"));		// Host
				sargs.add(new AussomInt(80));					// Port
				sargs.add(new AussomBool(true));				// Allow Self Signed
				AussomObject ao = (AussomObject) cls.instantiate(env, false, sargs);
				if (ao instanceof AussomException) {
					console.get().log(((AussomException) ao).stackTraceToString());
				} else {
					AussomSslSocket tsock = (AussomSslSocket) ao.getExternObject();
					tsock.setSocket(this.sock.accept());
				}
				return ao;
			} catch (aussomException e) {
				return new AussomException("sslServerSocket.accept(): Class 'sslSocket'.");
			} catch (IOException e) {
				return new AussomException("sslServerSocket.accept(): IO Exception. (" + e.getMessage() + ")");
			}
		} else
			return new AussomException("sslServerSocket.accept(): Class 'sslSocket' not found.");
	}

	public AussomType getCiphers(Environment env, ArrayList<AussomType> args) {
		AussomList suites = new AussomList();

		for(String s : ((SSLServerSocket)this.sock).getEnabledCipherSuites())
			suites.add(new AussomString(s));

		return suites;
	}

	public AussomType getProtocols(Environment env, ArrayList<AussomType> args) {
		AussomList protos = new AussomList();

		for(String s : ((SSLServerSocket)this.sock).getEnabledProtocols())
			protos.add(new AussomString(s));

		return protos;
	}

	public AussomType getEnableSessionCreation(Environment env, ArrayList<AussomType> args) {
		return new AussomBool(((SSLServerSocket)this.sock).getEnableSessionCreation());
	}

	public AussomType getNeedClientAuth(Environment env, ArrayList<AussomType> args) {
		return new AussomBool(((SSLServerSocket)this.sock).getNeedClientAuth());
	}

	public AussomType getUseClientMode(Environment env, ArrayList<AussomType> args) {
		return new AussomBool(((SSLServerSocket)this.sock).getUseClientMode());
	}

	public AussomType getWantClientAuth(Environment env, ArrayList<AussomType> args) {
		return new AussomBool(((SSLServerSocket)this.sock).getWantClientAuth());
	}

	public AussomType setCiphers(Environment env, ArrayList<AussomType> args) {
		AussomList suites = (AussomList)args.get(0);
		ArrayList<String> ss = new ArrayList<String>();
		for(AussomType n : suites.getValue()) {
			ss.add(n.getValueString());
		}
		((SSLServerSocket)this.sock).setEnabledCipherSuites((String[]) ss.toArray());

		return env.getClassInstance();
	}

	public AussomType setProtocols(Environment env, ArrayList<AussomType> args) {
		AussomList protos = (AussomList)args.get(0);
		ArrayList<String> ps = new ArrayList<String>();
		for(AussomType n : protos.getValue()) {
			ps.add(n.getValueString());
		}
		((SSLServerSocket)this.sock).setEnabledProtocols((String[]) ps.toArray());

		return env.getClassInstance();
	}

	public AussomType setEnableSessionCreation(Environment env, ArrayList<AussomType> args) {
		((SSLServerSocket)this.sock).setEnableSessionCreation(((AussomBool)args.get(0)).getValue());
		return env.getClassInstance();
	}

	public AussomType setNeedClientAuth(Environment env, ArrayList<AussomType> args) {
		((SSLServerSocket)this.sock).setNeedClientAuth(((AussomBool)args.get(0)).getValue());
		return env.getClassInstance();
	}

	public AussomType setUseClientMode(Environment env, ArrayList<AussomType> args) {
		((SSLServerSocket)this.sock).setUseClientMode(((AussomBool)args.get(0)).getValue());
		return env.getClassInstance();
	}

	public AussomType setWantClientAuth(Environment env, ArrayList<AussomType> args) {
		((SSLServerSocket)this.sock).setWantClientAuth(((AussomBool)args.get(0)).getValue());
		return env.getClassInstance();
	}
}
